import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/services/database.dart';
import 'package:flutterchat/widgets/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:random_string/random_string.dart';


class UpdateProfil extends StatefulWidget {
  @override
  _UpdateProfilState createState() => _UpdateProfilState();
}
String nameUpdate = '';
final _formKey = GlobalKey<FormState>();
final nameControler = TextEditingController();
final gameControler = TextEditingController();
final seriesControler = TextEditingController();
String picture ='';
String name = '';
String game = '';
String series = '';
String error = '';

class _UpdateProfilState extends State<UpdateProfil> {
  FirebaseUser user;
  File _image;
  final picker = ImagePicker();
  String downloadUrl = '';

  int get index => null;

  File _selectedImage;

  String getUserId(){
    if(user != null){
      return user.uid;
    }else{
      return 'pas id';
    }
  }

  void ClearText(){
    setState(() {
      nameControler.clear();
      gameControler.clear();
      seriesControler.clear();
    });
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.currentUser().then((FirebaseUser user) {
      setState(() { // call setState to rebuild the view
        this.user = user;
      });
    });
    return Scaffold(
      appBar: appBarMain("Update Profil"),
      backgroundColor: Constants.colorSecondary,
      body:  SingleChildScrollView(
          child: Container(
            alignment: Alignment.center,
            child: Form(
              key: _formKey,
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    Container(
                      width: MediaQuery.of(context).size.width > 800
                          ? 400
                          : MediaQuery.of(context).size.width - 60,
                      child: Column(
                        children: <Widget>[
                          imageProfil(context),
                          SizedBox(height: 20,),
                         GestureDetector(
                           onTap: (){
                             DatabaseMethods().updateData(name, game, series,downloadUrl, getUserId());
                             down(_image);
                           },
                           child: Container(
                             child: Text('Update Picture',
                             style: TextStyle(
                               color: Colors.blue,
                               fontSize: 20,
                             ),
                             ),
                           ),
                         ),
                          TextFormField(
                                controller: nameControler,
                                style: Constants.inputTextStyle(),
                                validator: (val) =>
                                val.isEmpty ? "Update your name" : null,
                                decoration:
                                Constants.themedecoration("name"),
                                onChanged: (val) {
                                  setState(() {
                                    name = val;
                                  });
                                },
                              ),
                          TextFormField(
                            controller: gameControler,
                            style: Constants.inputTextStyle(),
                            validator: (val) =>
                            val.isEmpty ? "Update your game" : null,
                            decoration:
                            Constants.themedecoration("Game favorite"),
                            onChanged: (val) {
                              setState(() {
                                game = val;
                              });
                            },
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          TextFormField(
                            controller: seriesControler,
                            style: Constants.inputTextStyle(),
                            textCapitalization: TextCapitalization.none,
                            validator: (val) =>
                            val.isEmpty ? "Update your series" : null,
                            decoration: Constants.themedecoration("Series prefer"),
                            onChanged: (val) {
                              series = val;
                            },
                          ),
                          SizedBox(
                            height: 24,
                          ),
                          GestureDetector(
                            onTap: () {
                              DatabaseMethods().updateData(name, game, series,downloadUrl, getUserId());
                              ClearText();
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width > 800
                                  ? 400
                                  : MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(
                                  vertical: 16, horizontal: 24),
                              decoration:   BoxDecoration(
                                  borderRadius: BorderRadius.circular(60),
                                  gradient: LinearGradient(
                                      colors: [Colors.blue, Colors.blueGrey
                                      ],
                                      begin: FractionalOffset.topRight,
                                      end: FractionalOffset.bottomLeft)),
                              child: Text(
                                "Update",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'OverpassRegular',
                                    fontWeight: FontWeight.w400,
                                    fontSize: 17),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 16,
                          ),
                          Container(
                            child: error.isEmpty
                                ? Container()
                                : Column(
                              children: <Widget>[
                                SizedBox(
                                  height: 8,
                                ),
                                Text(
                                  error,
                                  style: TextStyle(
                                      color: Colors.red, fontSize: 14),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 24,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
    );
  }

  Widget imageProfil(BuildContext context){
    return Stack(children: [
      CircleAvatar(
        radius: 90.0,
        backgroundImage: _image == null ? AssetImage('assets/images/update_profil.png') : NetworkImage(downloadUrl),
        backgroundColor: Colors.black12,
      ),
      Positioned(
        bottom: 10,
        right: 10,
        child: InkWell(
          onTap: (){
            showModalBottomSheet(
                context: context,
                builder: ((build) => BottomSheet(context))
            );
          },
          child: Icon(
            Icons.camera_alt,
            color: Colors.white,
            size: 28,
          ),
        ),
      ),
    ],);
  }

  Widget BottomSheet(BuildContext context){
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(children: [
        Text('Choose Profile photo',
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FlatButton.icon(onPressed: (){
              getImage(ImageSource.camera);
            },
                icon: Icon(Icons.camera),
                label: Text("Camera")),
            FlatButton.icon(onPressed: (){
              getImage(ImageSource.gallery);
            },
                icon: Icon(Icons.image),
                label: Text("Gallery")),
          ],
        ),
      ],),

    );
  }
  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
  void down(File _imagedown) async {
    String imageLocation = "image${randomAlphaNumeric(10)}.jpg";
    StorageReference blogImagesStorageReference = FirebaseStorage.instance
        .ref()
        .child("ProfilImages")
        .child(imageLocation);

    final StorageUploadTask task =
    blogImagesStorageReference.putFile(_imagedown);

    downloadUrl = await (await task.onComplete).ref.getDownloadURL();
    print("Profile Picture uploaded");

    _addPathToDatabase(imageLocation);

    print("url: " +"$downloadUrl");
  }


  Future uploadImage(BuildContext context) async {
    String fileName = basename(_image.path);
    String imageLocation = 'image${randomAlphaNumeric}.jpg';
    StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child("ProfilImages")
        .child(imageLocation);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    StorageTaskSnapshot taskSnapshot=await uploadTask.onComplete;
    setState(() {
      print("Profile Picture uploaded"+"$taskSnapshot");
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
    _addPathToDatabase(imageLocation);

  }

  Future<void> _addPathToDatabase(String text) async {
    try {
      // Get image URL from firebase
      final ref = FirebaseStorage().ref().child(text);
      var imageString = await ref.getDownloadURL();

      // Add location and url to database
      await Firestore.instance.collection('storage').document().setData({'url':imageString , 'location':text});
    }catch(e){
      print(e.message);
    }
  }
}



