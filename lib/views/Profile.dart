import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/helpers/data.dart';
import 'package:flutterchat/services/auth.dart';
import 'package:flutterchat/services/database.dart';
import 'package:flutterchat/services/search_service.dart';
import 'package:flutterchat/services/verify_email.dart';
import 'package:flutterchat/views/conversation_screen.dart';
import 'package:flutterchat/widgets/widgets.dart';

import 'home_container.dart';

String selectedAvatarUrl;

class Profile extends StatefulWidget {
  final Function toogleView;
  final String myAvatarFinal;

  Profile({this.toogleView, @required this.myAvatarFinal});

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final AuthService _authService = AuthService();
  final _formKey = GlobalKey<FormState>();

  // text feild
  bool _loading = false;
  String game = '';
  String series = '';
  String error = '';
  String _myAvatar = "";
  String _myName = "";
  String _myEmail = "";
  String _myGame = "";
  String _mySeries = "";

  List<String> avatarUrls = new List();

  @override
  void initState() {
    super.initState();

    avatarUrls = getAvatarUrls();
    avatarUrls.shuffle();
    selectedAvatarUrl = avatarUrls[0].toString();
    getMyInfoUser();
  }

  getMyInfoUser() async {
    _myAvatar = await Constants.getUserAvatarSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
    _myEmail = await Constants.getUserEmailSharedPreference();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Constants.colorPrimary));
    return Scaffold(
      backgroundColor: Constants.colorSecondary,
      appBar: appBarMain('Profil'),
      body: _loading
          ? Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      )
          : SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
            child: Container(
              height: MediaQuery.of(context).size.height,
              padding: EdgeInsets.only(top: 50),
              child: Column(
                children: <Widget>[
                  Container(
                      height: 90,
                      width: 90,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Constants.colorAccent, width: 2),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(120)),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(120),
                          child: kIsWeb
                              ? Image.network(_myAvatar)
                              : CachedNetworkImage(imageUrl: _myAvatar))
                  ),
                  SizedBox(
                    height: 80,
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 80),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text('Name',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 23,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(_myName,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            Text('Email',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 23,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(_myEmail,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            Text('Game',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 23,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(_myGame,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Row(
                          children: [
                            Text('Series',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 23,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(_mySeries,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
        ),
      ),
    );
  }
}
