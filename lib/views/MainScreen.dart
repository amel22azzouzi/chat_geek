import 'package:flutter/material.dart';
import 'package:flutterchat/views/profile_screen.dart';
import 'package:flutterchat/views/search.dart';
import 'package:flutterchat/views/update_profil.dart';
import 'home_container.dart';


class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  final bottomBar = [
    WebHome(),
    SearchScreen(),
    ProfileScreen(),
    UpdateProfil(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: bottomBar[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search),
              title: Text('Search'),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.message),
              title: Text('Messages'),
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profil'),
              backgroundColor: Colors.blue,
            ),
          ],
        onTap: (index){
            setState(() {
              _currentIndex = index;
            });
        },
      ),

    );
  }
}
