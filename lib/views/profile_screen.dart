

import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/models/home_container_model.dart';
import 'package:flutterchat/views/update_profil.dart';
import 'package:flutterchat/widgets/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';




class ProfileScreen extends StatefulWidget {
  final String userName;
  final String userAvatarUrl;
  final String userPicture;
  final String chatRoomId;
  ProfileScreen({this.userName, this.chatRoomId, this.userAvatarUrl,this.userPicture});

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

/// Defining some variable globally

HomeContainerModel homeContainerModel = new HomeContainerModel();
String _myName = "", _myAvatar = "";
String _myEmail = "";
String _myGame = "";
String _mySeries = "";
String _myPicture = "";

///
Stream infoStream;

/// creating a chat stream to listen
Stream messagesStreamHome;

class _ProfileScreenState extends State<ProfileScreen> {


  @override
  void initState() {

    getMyInfoAndChat();

    /// Stream
    if (infoStream == null) {
      infoStream =
      Stream<HomeContainerModel>.periodic(Duration(milliseconds: 100), (x) {
        return homeContainerModel;
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    infoStream = null;
    homeContainerModel.messagesStream = null;
    super.dispose();
  }


  getMyInfoAndChat() async {
    _myAvatar = await Constants.getUserAvatarSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
    _myEmail = await Constants.getUserEmailSharedPreference();
    _myGame = await Constants.getUserGameSharedPreference();
    _mySeries = await Constants.getUserSeriesSharedPreference();
  }

  /// Scaffold..........................

  @override
  Widget build(BuildContext context) {

    return StreamBuilder(
      stream: infoStream,
      builder: (context, snapshot) {
        //print("${snapshot.data.userNameGlobal}");
        return snapshot.hasData ? Scaffold(
          appBar: appBarMain('Profil'),
          body: Container(
            child: Home()
            //HomeContainerWidgets(),
          ),

        ) : Container();
      },
    );
  }

}


String getAvatarUrlOfOther(List<dynamic> avatarUrl) {
  if (avatarUrl[0] == _myAvatar) {
    return avatarUrl[1];
  } else {
    return avatarUrl[0];
  }
}

class Home extends StatefulWidget {
  Stream chats;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  FirebaseUser user;
  QuerySnapshot usersSnapshot;
  File _image;
  final picker = ImagePicker();


  getUserAvatar() async {
    //_myAvatarUrl = await Constants.getUserAvatarSharedPreference();
    _myEmail = await Constants.getUserEmailSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
  }

  String getUserId(){
    if(user != null){
      return user.uid;
    }else{
      return 'pas id';
    }
  }



  Widget imageProfil(BuildContext context, String pictureUrl){
    return Stack(children: [
      CircleAvatar(
        radius: 50.0,
        backgroundImage: _image == null ? AssetImage("assets/images/test.jpg") : Image.network(pictureUrl),
      ),
    ],);
  }





  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.currentUser().then((FirebaseUser user) {
      setState(() { // call setState to rebuild the view
        this.user = user;
      });
    });
    return Container(
      color: Color(0xff1F1F1F),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.only(top: 50),
                  child: Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: 80),
                        child : StreamBuilder(
                          stream: Firestore.instance.collection('users').snapshots(),
                          builder: (context, snapshot){
                            if(!snapshot.hasData) return Text('Loading...');
                            return ListView.builder(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                itemCount: snapshot.data.documents.length,
                                itemBuilder: (context, index) {
                                  return getUserId() == snapshot.data.documents[index].data["userId"]
                                      ? Profile_info(
                                    userId: getUserId(),
                                    userName: snapshot
                                        .data
                                        .documents[index]
                                        .data["userName"],
                                    email: snapshot
                                        .data
                                        .documents[index]
                                        .data["email"],
                                    game: snapshot
                                        .data
                                        .documents[index]
                                        .data["game"],
                                    series: snapshot
                                        .data
                                        .documents[index]
                                        .data["series"],
                                    pictureUrl: snapshot
                                                .data
                                                .documents[index]
                                                .data["avatarUrl"],
                                  )
                                      :Container();
                                });

                          }
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    UpdateProfil(),
                              ));
                        },
                        child: Container(
                          width: 200,
                          padding: EdgeInsets.symmetric(
                              vertical: 16, horizontal: 24),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(60),
                              gradient: LinearGradient(
                                  colors: [Colors.blue, Colors.blueGrey
                                  ],
                                  begin: FractionalOffset.topRight,
                                  end: FractionalOffset.bottomLeft)),
                          child: Text(
                            "Update Profil",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'OverpassRegular',
                                fontWeight: FontWeight.w400,
                                fontSize: 17),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Profile_info extends StatelessWidget {
  final String userName, game, series, email,pictureUrl, userId;
  Profile_info(
      {this.userName,
        this.game,
        this.series,
        this.email,
        this.pictureUrl,
        this.userId,
        });
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(right: 70),
          child: Column(
            children: <Widget>[
              Container(
                child: CircleAvatar(
                  radius: 60.0,
                  backgroundImage: NetworkImage(pictureUrl),
                ),
              ),
              SizedBox(height: 20,),
            ],
          ),
        ),

        Row(
          children: [
            Text(userId,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
            SizedBox(width: 45),
          ],
        ),
        Row(
          children: [
            Icon(
              Icons.face,
              color: Colors.blueGrey,
              size: 25,
            ),
            SizedBox(width: 20),
            Text('Name',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 25,
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(width: 45),
            Text(userName,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          children: [
            Icon(
              Icons.email,
              color: Colors.blueGrey,
              size: 25,
            ),
            SizedBox(width: 20),
            Text('Email',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 25,
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(width: 45),
            Text(email,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          children: [
            Icon(
              Icons.videogame_asset,
              color: Colors.blueGrey,
              size: 25,
            ),
            SizedBox(width: 20),
            Text('Favorite Game',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 25,
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(width: 45),
            Text(game,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 30,
        ),
        Row(
          children: [
            Icon(
              Icons.filter_b_and_w,
              color: Colors.blueGrey,
              size: 25,
            ),
            SizedBox(width: 20),
            Text('Favorite Series',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: 25,
              ),
            ),
          ],
        ),
        Row(
          children: [
            SizedBox(width: 45),
            Text(series,
              style: TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 40,
        ),
      ],
    );
  }
}


