import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/helpers/data.dart';
import 'package:flutterchat/services/auth.dart';
import 'package:flutterchat/services/database.dart';
import 'package:flutterchat/views/profile_screen.dart';
import 'package:flutterchat/widgets/widgets.dart';

import 'home_container.dart';

String selectedAvatarUrl;

class UserInfo extends StatefulWidget {
  final Function toogleView;
  final String myAvatarFinal;

  UserInfo({this.toogleView, @required this.myAvatarFinal});

  @override
  _UserInfoState createState() => _UserInfoState();
}

class _UserInfoState extends State<UserInfo> {
  final AuthService _authService = AuthService();
  final _formKey = GlobalKey<FormState>();
  final gameConroler = TextEditingController();
  final seriesConroler = TextEditingController();
  FirebaseUser user;


  // text feild
  bool _loading = false;
  String game = '';
  String series = '';
  String error = '';
  String _myAvatar = "";
  String _myName = "";
  String _myGame = "";
  String _mySeries = "";


  List<String> avatarUrls = new List();


  @override
  void initState() {
    super.initState();

    avatarUrls = getAvatarUrls();
    avatarUrls.shuffle();
    selectedAvatarUrl = avatarUrls[0].toString();
    getMyInfoUser();
  }

  getMyInfoUser() async {
    _myAvatar = await Constants.getUserPictureSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
  }
  String getUserId(){
    if(user != null){
      return user.uid;
    }else{
      return 'pas id';
    }
  }


  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance.currentUser().then((FirebaseUser user) {
      setState(() { // call setState to rebuild the view
        this.user = user;
      });
    });

    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Constants.colorPrimary));
    return Scaffold(
      backgroundColor: Constants.colorSecondary,
      appBar: appBarMain('Profil'),
      body: _loading
          ? Container(
        alignment: Alignment.center,
        child: CircularProgressIndicator(),
      )
          : SingleChildScrollView(
        child: Container(
          alignment: Alignment.center,
          child: Form(
            key: _formKey,
            child: Container(
              height: MediaQuery.of(context).size.height - 200,
              padding: EdgeInsets.only(top: 30, bottom: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                      height: 90,
                      width: 90,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Constants.colorAccent, width: 2),
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(120)),
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(120),
                          child: kIsWeb
                              ? Image.network(_myAvatar)
                              : CachedNetworkImage(imageUrl: _myAvatar))
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    child: Text(_myName,
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 20,
                    ),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width > 800
                        ? 400
                        : MediaQuery.of(context).size.width - 60,
                    child: Column(
                      children: <Widget>[
                        ///
                        TextFormField(
                          controller: gameConroler,
                          style: Constants.inputTextStyle(),
                          validator: (val) =>
                          val.isEmpty ? "Enter an game" : null,
                          decoration:
                          Constants.themedecoration("Game favorite"),
                          onChanged: (val) {
                            setState(() {
                              game = val;
                            });
                          },
                        ),
                        SizedBox(
                          height: 8,
                        ),
                        TextFormField(
                          controller: seriesConroler,
                          style: Constants.inputTextStyle(),
                          textCapitalization: TextCapitalization.none,
                          validator: (val) =>
                          val.isEmpty ? "Enter an Series" : null,
                          decoration: Constants.themedecoration("Series prefer"),
                          onChanged: (val) {
                              series = val;
                          },
                        ),
                        SizedBox(
                          height: 24,
                        ),
                        GestureDetector(
                          onTap: () {
                            DatabaseMethods().userInfo(gameConroler.text, seriesConroler.text,getUserId());
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => WebHome()),
                            );
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width > 800
                                ? 400
                                : MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 24),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60),
                                gradient: LinearGradient(
                                    colors: [Colors.red, Colors.blueGrey
                                    ],
                                    begin: FractionalOffset.topRight,
                                    end: FractionalOffset.bottomLeft)),
                            child: Text(
                              "Next",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: 'OverpassRegular',
                                  fontWeight: FontWeight.w400,
                                  fontSize: 17),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Container(
                          child: error.isEmpty
                              ? Container()
                              : Column(
                            children: <Widget>[
                              SizedBox(
                                height: 8,
                              ),
                              Text(
                                error,
                                style: TextStyle(
                                    color: Colors.red, fontSize: 14),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 24,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
