
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutterchat/helpers/authenticate.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/helpers/helper_functions.dart';
import 'package:flutterchat/models/home_container_model.dart';
import 'package:flutterchat/services/auth.dart';
import 'package:flutterchat/services/database.dart';
import 'package:flutterchat/services/search_service.dart';
import 'package:flutterchat/views/profile_screen.dart';
import 'package:flutterchat/widgets/widgets.dart';

import 'chatinfo.dart';
import 'conversation_screen.dart';
import 'home_container.dart';


class SearchScreen extends StatefulWidget {

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

/// Defining some variable globally

HomeContainerModel homeContainerModel = new HomeContainerModel();
String _myName = "", _myAvatar = "";
String _myEmail = "";

///
Stream infoStream;

/// creating a chat stream to listen
Stream messagesStreamHome;

class _SearchScreenState extends State<SearchScreen> {
  Stream chats;

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  @override
  void initState() {
    getMyInfoAndChat();
    /// Stream
    if (infoStream == null) {
      infoStream =
      Stream<HomeContainerModel>.periodic(Duration(milliseconds: 100), (x) {
        return homeContainerModel;
      });
    }
    super.initState();
  }

  @override
  void dispose() {
    infoStream = null;
    homeContainerModel.messagesStream = null;
    super.dispose();
  }
  getMyInfoAndChat() async {
    _myAvatar = await Constants.getUserAvatarSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
    _myEmail = await Constants.getUserEmailSharedPreference();
    //print("this is uservavatar $_myName");
    //print("Filling up some dat $_myName");
  }
  Widget menu({context, double menuWidth}) {
    return Container(
      decoration: BoxDecoration(color: Colors.blueGrey),
      width: menuWidth ?? MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 60,
          ),
          Container(
              padding: EdgeInsets.only(left: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProfileScreen()),
                      );
                    },
                    child: Container(
                        height: 90,
                        width: 90,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: Constants.colorAccent, width: 2),
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(120)),
                        child: ClipRRect(
                            borderRadius: BorderRadius.circular(120),
                           child: kIsWeb
                                ? Image.network(_myAvatar)
                                : CachedNetworkImage(imageUrl: _myAvatar)
                           )
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    _myName,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'OverpassRegular',
                    ),
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    _myEmail,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                      fontFamily: 'OverpassRegular',
                    ),
                  ),
                ],
              )),
          SizedBox(
            height: 30,
          ),
          GestureDetector(
            onTap: () {
            },
            child: Container(
              padding: EdgeInsets.only(left: 30),
              child: Row(
                children: <Widget>[
                  Image.asset(
                    "assets/images/home.png",
                    height: 18,
                    width: 18,
                  ),
                  SizedBox(
                    width: 16,
                  ),
                  Text(
                    "Home",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontFamily: 'OverpassRegular',
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: 16,
          ),
          GestureDetector(
            onTap: (){
              _aboutUsInfo(context);
            },
            child: Opacity(
              opacity: 0.8,
              child: Container(
                padding: EdgeInsets.only(left: 30),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "assets/images/info.png",
                      height: 18,
                      width: 18,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "About Us",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: 'OverpassRegular',
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Spacer(),
          GestureDetector(
            onTap: () {
              AuthService().signOut();
              Constants.saveUserLoggedInSharedPreference(false);
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => Authenticate()));
            },
            child: Opacity(
              opacity: 0.8,
              child: Container(
                padding: EdgeInsets.only(left: 30),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/logout.png",
                      height: 20,
                      width: 20,
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Text(
                      "Log out",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontFamily: 'OverpassRegular',
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            height: 80,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {

    return StreamBuilder(
      stream: infoStream,
      builder: (context, snapshot) {
        //print("${snapshot.data.userNameGlobal}");
        return snapshot.hasData ? Scaffold(
          appBar: appBarMain('Search Geek'),
          drawer: Drawer(child: menu(context: context)),
          body: Home()
            //HomeContainerWidgets(),
        ) : Container();
      },
    );
  }

}
String getAvatarUrlOfOther(List<dynamic> avatarUrl) {
  if (avatarUrl[0] == _myAvatar) {
    return avatarUrl[1];
  } else {
    return avatarUrl[0];
  }
}
class Home extends StatefulWidget {
  final BuildContext context;
  final double width;
  Stream chats;
  final GlobalKey<ScaffoldState> homeScaffoldKey;

  Home(
      {@required this.context,
        @required this.width,
        @required this.chats,
        @required this.homeScaffoldKey});

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  bool isLoading = false;
  String searchString = "";
  QuerySnapshot querySnapshot;

  QuerySnapshot usersSnapshot;
  String _myName = "", _myAvatar = "";
  String _myEmail = "";




  getUserAvatar() async {
    //_myAvatarUrl = await Constants.getUserAvatarSharedPreference();
    _myEmail = await Constants.getUserEmailSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
    setState(() {});
  }
  getRecentMembers() {
    DatabaseMethods().getRecentUsers().then((snapshot) {
      print(snapshot.documents[0].data['userName'].toString()+"this is awesome");
      setState(() {
        usersSnapshot = snapshot;
      });
    });
  }
  initiateSearch() {
    getUserAvatar();

    setState(() {
      isLoading = true;
    });
    print("initiated the search with : $searchString");
    setState(() {
      isLoading = true;
    });

    SearchService().searchByName(searchString).then((result) {
      setState(() {
        querySnapshot = result;
        print("${querySnapshot.documents.length}");
        print(("this is ${querySnapshot.documents[0].toString()}"));
        setState(() {
          isLoading = false;
        });
      });
    });
  }

  Widget recentUserList(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 30,),
        Container(
          padding: EdgeInsets.symmetric(horizontal:  16),
          child: Text(
            "Recent Members of Geek Chat",
            style: TextStyle(
              color: Colors.white,
              fontSize: 15,
              fontFamily: 'OverpassRegular',
            ),
          ),
        ),
        Container(
          child: usersSnapshot != null
              ? ListView.builder(
              itemCount: usersSnapshot.documents.length,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return userTile(
                  userEmail: usersSnapshot.documents[index].data['email'],
                  userName:
                  usersSnapshot.documents[index].data['userName'],
                  userAvatarUrl:
                  usersSnapshot.documents[index].data['avatarUrl'],
                );
              })
              : Container(),
        ),
      ],
    );
  }

  Widget userList() {
    return isLoading
        ? Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Center(
        child: CircularProgressIndicator(),
      ),
    )
        : Container(
      child: querySnapshot != null
          ? ListView.builder(
          itemCount: querySnapshot.documents.length,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return userTile(
              userEmail: querySnapshot.documents[index].data['email'],
              userName:
              querySnapshot.documents[index].data['userName'],
              userAvatarUrl:
              querySnapshot.documents[index].data['avatarUrl'],
            );
          })
          : Container(),
    );
  }


  @override
  void initState() {
    getRecentMembers();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xff1F1F1F),
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: <Widget>[
              SingleChildScrollView(
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  padding: EdgeInsets.only(top: 50),
                  child: Column(
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width > 800 ? 300 : MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding:
                              EdgeInsets.symmetric(horizontal: 8, vertical: 1),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(60),
                              ),
                              child: Row(
                                children: <Widget>[
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Expanded(
                                    child: Container(
                                      margin: EdgeInsets.only(right: 16),
                                      child: TextField(
                                        // controller: textController,
                                        onChanged: (val) {
                                          searchString = val;
                                        },
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 15),
                                        decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "search username ...",
                                          hintStyle: TextStyle(
                                              color: Colors.white, fontSize: 16),
                                        ),
                                        maxLines: 1,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      initiateSearch();
                                    },
                                    child: Container(
                                      width: 40,
                                      height: 40,
                                      child: Image.asset(
                                        "assets/images/search_white.png",
                                        width: 25,
                                        height: 25,
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(25),
                                          gradient: LinearGradient(
                                              colors: [
                                                const Color(0x36FFFFFF),
                                                const Color(0x0FFFFFFF)
                                              ],
                                              begin: FractionalOffset.topLeft,
                                              end: FractionalOffset.bottomRight)),
                                      padding: EdgeInsets.all(12),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                ],
                              ),
                            ),
                            /// List.....
                            userList(),
                            recentUserList(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }

}

class ItsLargeHome extends StatefulWidget {
  final Stream chats;
  final String userName;
  final String userPicUrl;
  final String chatRoomId;
  final scaffoldKey;

  ItsLargeHome(
      {@required this.chats,
        @required this.chatRoomId,
        @required this.userPicUrl,
        @required this.userName,
        @required this.scaffoldKey});

  @override
  _ItsLargeHomeState createState() => _ItsLargeHomeState();
}

class _ItsLargeHomeState extends State<ItsLargeHome> {
  @override
  Widget build(BuildContext context) {
    /*  databaseMethods.getChats(widget.chatRoomId).then((result) {
          messagesStreamHome = result;
          /// setting up the profile pic name and email
          homeContainerModel.userPicUrlGlobal = widget.userPicUrl;
          homeContainerModel.userNameGlobal = widget.userName;
          // homeContainerModel.userEmailGlobal = userEmail;
          setState(() {});
        });*/
    return Row(
      children: <Widget>[
        Expanded(
            child: Home(
                context: context,
                width: 300,
                chats: widget.chats,
                homeScaffoldKey: widget.scaffoldKey)),
        homeContainerModel.isChatSelected
            ? ConversationScreen(
          width: MediaQuery.of(context).size.width - 300,
          name: homeContainerModel.userNameGlobal,
          profilePicUrl: homeContainerModel.userPicUrlGlobal,
          chatRoomid: homeContainerModel.chatRoomIdGlobal,
          messagesStream: homeContainerModel.messagesStream,
          myNameFinal: homeContainerModel.userNameGlobal,
          //TODO
          myEmail: "",
          myAvatarFinal: homeContainerModel.userPicUrlGlobal,
          scaffoldKey: widget.scaffoldKey,
        )
            : Container(
          width: kIsWeb ? MediaQuery.of(context).size.width - 300 : 0,
        ),
      ],
    );
  }
}

Widget userTile({String userName, userEmail, String userAvatarUrl}) {
  return Container(
    width: 300,
    margin: EdgeInsets.symmetric(vertical: 12),
    padding: EdgeInsets.symmetric(horizontal: 16),
    child: Row(
      children: <Widget>[
        Container(
            padding: EdgeInsets.all(8),
            height: 40,
            width: 40,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(40)),
            child: Image.network(userAvatarUrl)),
        SizedBox(
          width: 8,
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              userName,
              style: TextStyle(color: Colors.white, fontSize: 15),
            ),
            Text(
              userEmail,
              style: TextStyle(color: Colors.white, fontSize: 12),
            ),
          ],
        ),
        Spacer(),
        GestureDetector(
          onTap: () async {
            if (userName != _myName) {
              List<String> users = [_myName, userName];

              List<String> avatarUrls = [_myAvatar, userAvatarUrl];

              List<int> unreadMessages = [0, 1];

              String chatRoomid =
              getChatRoomId(userName, _myName); // "$userName\_${myName}";

              /// Create Chat Room
              Map<String, dynamic> chatRoom = {
                "users": users,
                "avatarUrl": avatarUrls,
                "lastmessage": "Hey",
                "lastMessageSendBy": _myName,
                'timestamp': Timestamp.now(),
                "unreadMessage": unreadMessages,
                "chatRoomId": chatRoomid
              };

              bool canMessage;
              DatabaseMethods().addChatRoom(chatRoom, chatRoomid).then((val) {
                canMessage = val;
                if (canMessage) {
                  homeContainerModel.chatRoomIdGlobal = chatRoomid;
                  databaseMethods.getChats(chatRoomid).then((result) {
                    messagesStreamHome = result;
                    homeContainerModel.messagesStream = result;
                    homeContainerModel.isSearching = false;

                    /// setting up the profile pic name and email
                    homeContainerModel.userPicUrlGlobal = userAvatarUrl;
                    homeContainerModel.userNameGlobal = userName;
                    homeContainerModel.userEmailGlobal = userEmail;

                    print(
                        "${homeContainerModel.messagesStream}   $messagesStreamHome");
                    homeContainerModel.isChatSelected = true;
                  });
                  /* Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        */ /*
                                userName: userName, userAvatarUrl: userAvatarUrl, chatRoomid,messagesStream,*/ /*
                          builder: (context) => WebHome(
                            userName: userName,
                            userAvatarUrl: userAvatarUrl,
                            chatRoomId: chatRoomid,
                          )));*/
                }
              });

              /// sending to chat

            } else {
              // TODO show snackbar you cannot send message to yourself
            }

          },
          // Message button
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(60),
                color: Colors.blueGrey,
              ),
              child: Text(
                "Message",
                style: TextStyle(color: Colors.white),
              )),
        )
      ],
    ),
  );
}

Future<void> _aboutUsInfo(BuildContext context) async {
  return showDialog<void>(
    context: context,
    barrierDismissible: true, // user must tap button!
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text('More Options'),
        content: Text(
            ' © 2021 Create By TANEFLIT'),
        actions: <Widget>[
          FlatButton(
            child: Text('See More'),
            onPressed: () {
              HelperFunctions.launchURL("https://www.taneflit.com/");
            },
          ),
          FlatButton(
            child: Text('Cancel'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}


/*
ItsLargeHome(
              chats: chats,
              userName: homeContainerModel.userNameGlobal,
              userPicUrl: homeContainerModel.userPicUrlGlobal,
              chatRoomId: homeContainerModel.chatRoomIdGlobal,
              scaffoldKey: _scaffoldKey,
            );
 */