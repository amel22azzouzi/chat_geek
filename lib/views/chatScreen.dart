import 'package:flutter/material.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/services/database.dart';

import 'conversation_screen.dart';
import 'home_container.dart';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  String _myName = "", _myAvatar = "";
  String _myEmail = "";
  Stream chats;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  getMyInfoAndChat() async {
    _myAvatar = await Constants.getUserAvatarSharedPreference();
    _myName = await Constants.getUserNameSharedPreference();
    _myEmail = await Constants.getUserEmailSharedPreference();
    //print("this is uservavatar $_myName");
    //print("Filling up some dat $_myName");
    DatabaseMethods().getUserChats(_myName).then((value) {
      chats = value;
      //if (!mounted) return;
      setState(() {});
    });
  }
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: ConversationScreen(
          width: MediaQuery.of(context).size.width,
          name: homeContainerModel.userNameGlobal,
          profilePicUrl:
          homeContainerModel.userPicUrlGlobal,
          chatRoomid:
          homeContainerModel.chatRoomIdGlobal,
          messagesStream:
          homeContainerModel.messagesStream,
          myNameFinal: _myName,
          myEmail: _myEmail,
          myAvatarFinal: _myAvatar,
          scaffoldKey: _scaffoldKey,
        ),
    );
  }
}
