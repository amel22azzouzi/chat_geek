
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutterchat/helpers/constants.dart';
import 'package:flutterchat/helpers/data.dart';
import 'package:flutterchat/services/auth.dart';
import 'package:flutterchat/services/database.dart';
import 'package:flutterchat/views/home_container.dart';
import 'package:flutterchat/services/email_auth.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:random_string/random_string.dart';



String selectedAvatarUrl;

class SignUp extends StatefulWidget {
  final Function toogleView;

  SignUp({this.toogleView});

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final AuthService _authService = AuthService();
  final _formKey = GlobalKey<FormState>();
  bool submitValid = false;
  final _emailConroler = TextEditingController();
  final _otpcontroller = TextEditingController();
  FirebaseUser user;

  // text feild
  bool _loading = false;
  String email = '', password = '', name = "";
  String error = '';
  String game = '';
  String series = '';

  List<String> avatarUrls = new List();

  File _image;
  final picker = ImagePicker();
  String downloadUrl = '';


  @override
  void initState() {
    super.initState();

    avatarUrls = getAvatarUrls();
    avatarUrls.shuffle();
    selectedAvatarUrl = avatarUrls[0].toString();
  }
  void verify() {
    print(EmailAuth.validate(
        receiverMail: _emailConroler.value.text,
        userOTP: _otpcontroller.value.text));
  }
  void sendOtp() async {
    EmailAuth.sessionName = "The Geek Chat";
    bool result =
    await EmailAuth.sendOtp(receiverMail: _emailConroler.value.text);
    if (result) {
      setState(() {
        submitValid = true;
      });
    }
  }
  String getUserId(){
      return user.uid;
  }

  Widget avatarTile(String avatarUrl, _SignUpState context, int index) {
    return GestureDetector(
      onTap: () {
        selectedAvatarUrl = avatarUrl;
        setState(() {});
      },
      child: Container(
          padding: EdgeInsets.all(8),
          margin: EdgeInsets.only(
            right: 14,
            left: index == 0 ? 30 : 0,
          ),
          height: 100,
          width: 100,
          decoration: BoxDecoration(
              border: selectedAvatarUrl == avatarUrl
                  ? Border.all(color: Color(0xff007EF4), width: 4)
                  : Border.all(color: Colors.transparent, width: 10),
              color: Colors.white,
              borderRadius: BorderRadius.circular(120)),
          child: kIsWeb
              ? Image.network(avatarUrl)
              : CachedNetworkImage(imageUrl: avatarUrl)),
    );
  }

  Widget imageProfil(BuildContext context){
    return Stack(children: [
      CircleAvatar(
        radius: 90.0,
        backgroundImage: _image == null ? AssetImage("assets/images/test.jpg") : FileImage(File(_image.path)),
      ),
      Positioned(
        bottom: 10,
        right: 10,
        child: InkWell(
          onTap: (){
            showModalBottomSheet(
                context: context,
                builder: ((build) => BottomSheet(context))
            );
          },
          child: Icon(
            Icons.camera_alt,
            color: Colors.white,
            size: 28,
          ),
        ),
      ),
    ],);
  }

  Widget BottomSheet(BuildContext context){
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 20,
      ),
      child: Column(children: [
        Text('Choose Profile photo',
          style: TextStyle(
            fontSize: 20.0,
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            FlatButton.icon(onPressed: (){
              getImage(ImageSource.camera);
            },
                icon: Icon(Icons.camera),
                label: Text("Camera")),
            FlatButton.icon(onPressed: (){
              getImage(ImageSource.gallery);
            },
                icon: Icon(Icons.image),
                label: Text("Gallery")),
          ],
        ),
      ],),

    );
  }
  Future getImage(ImageSource source) async {
    final pickedFile = await picker.getImage(source: source);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }
  void down(File _imagedown) async {
    String imageLocation = "image${randomAlphaNumeric(10)}.jpg";
    StorageReference blogImagesStorageReference = FirebaseStorage.instance
        .ref()
        .child("ProfilImages")
        .child(imageLocation);

    final StorageUploadTask task =
    blogImagesStorageReference.putFile(_imagedown);

    downloadUrl = await (await task.onComplete).ref.getDownloadURL();
    print("Profile Picture uploaded");

   // _addPathToDatabase(imageLocation);

    print("url: " +"$downloadUrl");
  }

  @override
  Widget build(BuildContext context) {

    FirebaseAuth.instance.currentUser().then((FirebaseUser user) {
      setState(() { // call setState to rebuild the view
        this.user = user;
      });
    });
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Constants.colorPrimary));

    return Scaffold(
      backgroundColor: Constants.colorSecondary,
     // appBar: appBarMain(context),
      body: _loading
          ? Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("assets/images/gif5.gif"),
                    fit: BoxFit.cover,
                  ),
                ),
                alignment: Alignment.center,
                child: Form(
                  key: _formKey,
                  child: Container(
                    height: MediaQuery.of(context).size.height,
                    padding: EdgeInsets.only(top: 40, bottom: 8),
                    child: SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            height: 100,
                            margin: EdgeInsets.only(bottom: 5),
                            child: imageProfil(context),

                          ),
                          Container(
                            width: MediaQuery.of(context).size.width > 880
                                ? 300
                                : MediaQuery.of(context).size.width - 60,
                            child: Column(
                              children: <Widget>[
                                ///
                                TextFormField(
                                  style: Constants.inputTextStyle(),
                                  validator: (val) =>
                                      val.isEmpty ? "Enter an Name" : null,
                                  decoration:
                                      Constants.themedecoration("username"),
                                  onChanged: (val) {
                                    setState(() {
                                      name = val.toLowerCase();
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                TextFormField(
                                  controller: _emailConroler,
                                  style: Constants.inputTextStyle(),
                                  textCapitalization: TextCapitalization.none,
                                  validator: (val) =>
                                      val.isEmpty ? "Enter an Email" : null,
                                  decoration: Constants.themedecoration("email"),
                                  onChanged: (val) {
                                    setState(() {
                                      email = val;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                TextFormField(
                                  style: Constants.inputTextStyle(),
                                  validator: (val) =>
                                  val.isEmpty ? "Enter an Game" : null,
                                  decoration:
                                  Constants.themedecoration("favorite game"),
                                  onChanged: (val) {
                                    setState(() {
                                      game = val;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                TextFormField(
                                  style: Constants.inputTextStyle(),
                                  validator: (val) =>
                                  val.isEmpty ? "Enter an Series" : null,
                                  decoration:
                                  Constants.themedecoration("favorite series"),
                                  onChanged: (val) {
                                    setState(() {
                                      series = val;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                TextFormField(
                                  style: Constants.inputTextStyle(),
                                  validator: (val) => val.length > 6
                                      ? null
                                      : "Please Enter "
                                          "Password more than 6 character",
                                  decoration:
                                      Constants.themedecoration("password"),
                                  obscureText: true,
                                  onChanged: (val) {
                                    setState(() {
                                      password = val;
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                GestureDetector(
                                  onTap: sendOtp,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width > 800
                                        ? 400
                                        : MediaQuery.of(context).size.width,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 16, horizontal: 24),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(60),
                                        gradient: LinearGradient(
                                            colors: [Colors.blue, Colors.blueGrey
                                            ],
                                            begin: FractionalOffset.topRight,
                                            end: FractionalOffset.bottomLeft)),
                                    child: Text(
                                      "Sign Up",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontFamily: 'OverpassRegular',
                                          fontWeight: FontWeight.w400,
                                          fontSize: 17),
                                    ),
                                  ),
                                ),
                                (submitValid)
                                    ? TextField(
                                  controller: _otpcontroller,
                                  style: Constants.inputTextStyle(),
                                  textCapitalization: TextCapitalization.none,
                                  decoration: Constants.themedecoration("Verification code"),
                                )
                                    : Container(height: 20),
                                (submitValid)
                                    ? Container(
                                  child: GestureDetector(
                                    onTap:() async {
                                      verify;
                                      if(EmailAuth.validate(
                                          receiverMail: _emailConroler.value.text,
                                          userOTP: _otpcontroller.value.text)){
                                         if (_formKey.currentState.validate()) {
                                          setState(() {
                                            _loading = true;
                                          });
                                          await _authService
                                              .signUpWithEmailAndPassword(
                                              email, password)
                                              .then((value) {
                                            if (value != null) {
                                              /// uploading user info to Firestore
                                              Map<String, String> userInfo = {
                                                "userName": name,
                                                "email": email,
                                                "avatarUrl": downloadUrl,
                                                "game": game,
                                                "picture": downloadUrl,
                                                "series": series,
                                                "userId": getUserId(),
                                              };
                                              DatabaseMethods()
                                                  .addData(userInfo)
                                                  .then((result) {});
                                              Constants
                                                  .saveUserLoggedInSharedPreference(
                                                  true);
                                              Constants.saveUserNameSharedPreference(
                                                  name);
                                              print("$name username saved");
                                              Constants
                                                  .saveUserPictureSharedPreference(
                                                  selectedAvatarUrl);
                                              print(
                                                  "$selectedAvatarUrl user avatar saved");
                                              Constants
                                                  .saveUserPictureSharedPreference(
                                                  downloadUrl);
                                              print(
                                                  "$downloadUrl user picture saved");
                                              Constants.saveUserEmailSharedPreference(
                                                  email);
                                              print("$email user email saved");
                                              setState(() {
                                                _loading = false;
                                              });
                                              Constants.saveUserGameSharedPreference(
                                                  game);
                                              print("$game usergame saved");
                                              Constants.saveUserSeriesSharedPreference(
                                                    series);
                                              print("$series userseries saved");
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                    builder: (context) =>
                                                        WebHome(),
                                                  ));
                                            }
                                            else{
                                              setState(() {
                                                _loading = false;
                                                error = "please supply a valid/another email";
                                              });
                                            }
                                          });
                                        }
                                      }
                                      down(_image);

                                    },
                                    child: Center(
                                      child: Container(
                                        width: MediaQuery.of(context).size.width > 800
                                            ? 400
                                            : MediaQuery.of(context).size.width,
                                        margin: EdgeInsets.only(top: 20),
                                        padding: EdgeInsets.symmetric(
                                            vertical: 16, horizontal: 24),
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(60),
                                            gradient: LinearGradient(
                                                colors: [Colors.blue, Colors.blueGrey
                                                ],
                                                begin: FractionalOffset.topRight,
                                                end: FractionalOffset.bottomLeft)),
                                        child: Text(
                                          "Verification",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: 'OverpassRegular',
                                              fontWeight: FontWeight.w400,
                                              fontSize: 17),
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                                    : SizedBox(height: 16),
                                SizedBox(
                                  height: 16,
                                ),
                                GestureDetector(
                                   onTap: () async {
                                  //   await _authService.signInWithGoogle(context);
                                  //   Navigator.push(
                                  //       context,
                                  //       MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             UserInfo(),
                                  //       ));
                                  },
                                  child: Container(
                                    width: MediaQuery.of(context).size.width > 800
                                        ? 400
                                        : MediaQuery.of(context).size.width - 60,
                                    padding: EdgeInsets.symmetric(
                                        vertical: 16, horizontal: 30),
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30))),
                                    child: Text(
                                      "Sign Up with Google",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Color(0xff071930),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: 'OverpassRegular',
                                          fontSize: 17),
                                    ),
                                  ),
                                ),
                                Container(
                                  child: error.isEmpty
                                      ? Container()
                                      : Column(
                                    children: <Widget>[
                                      SizedBox(
                                        height: 8,
                                      ),
                                      Text(
                                        error,
                                        style: TextStyle(
                                            color: Colors.red, fontSize: 14),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 24,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      "Already have and account?",
                                      style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16,
                                        fontFamily: 'OverpassRegular',
                                      ),
                                    ),
                                    SizedBox(
                                      width: 4,
                                    ),
                                    GestureDetector(
                                        onTap: () {
                                          widget.toogleView();
                                        },
                                        child: Text(
                                          "Sign In",
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontFamily: 'OverpassRegular',
                                            decoration: TextDecoration.underline,
                                          ),
                                        ))
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }
}
