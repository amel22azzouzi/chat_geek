import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutterchat/helpers/authenticate.dart';
import 'package:flutterchat/views/home_container.dart';
import 'package:shimmer/shimmer.dart';

import 'helpers/constants.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool userIsLoggedIn;

  @override
  void initState() {
    getLoggedInState();
    super.initState();
    Timer(
      Duration(seconds: 3),
          () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => userIsLoggedIn != null ? userIsLoggedIn ? WebHome() : Authenticate() : Container(),
        ),
      ),
    );
  }

  getLoggedInState() async {
    await Constants.getUserLoggedInSharedPreference().then((value) {
      setState(() {
        userIsLoggedIn = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Image(
             image: AssetImage('assets/images/gif5.gif'),
             height: 800.0,
             fit: BoxFit.fitHeight,
           ),
              Shimmer.fromColors(
                period: Duration(seconds: 2),
                baseColor: Colors.blue,
                highlightColor: Colors.white,
                child: Text(
                  'The Geek Chat',
                  style: TextStyle(
                    fontSize: 42.0,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                    shadows: <Shadow>[
                      Shadow(
                        color: Colors.black87,
                        blurRadius: 20.0,
                        offset: Offset.fromDirection(125.15),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}